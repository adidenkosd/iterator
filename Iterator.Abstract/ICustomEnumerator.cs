﻿namespace Iterator.Abstract
{
	/// <summary>
	/// Interface, for custom iterator pattern.
	/// </summary>
	public interface ICustomEnumerator<out T>
	{
		bool MoveNext();

		T Current { get; }

		void Reset();
	}
}