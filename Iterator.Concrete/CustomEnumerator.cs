﻿namespace Iterator.Concrete
{
	using System.Collections.Generic;
	using Abstract;
	public class CustomEnumerator<T> : ICustomEnumerator<T>
	{
		private readonly T[] _items;
		private int _index;

		public CustomEnumerator(T[] items) {
			_items = items;
		}

		public bool MoveNext() {
			if (_index >= _items.Length - 1) {
				Reset();
				return false;
			}
			_index++;
			return true;
		}

		public T Current => _items[_index];

		public void Reset() {
			_index = 0;
		}
	}
}