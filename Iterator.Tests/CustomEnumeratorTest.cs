using System;
using Xunit;

namespace Iterator.Tests
{
	using Abstract;
	using Concrete;

	public class CustomEnumeratorTest
	{
		[Theory]
		[InlineData(new[] { 1, 2, 3, 4, 5, 6 })]
		public void CanMoveNextIfNextElementExists(int[] items) {
			ICustomEnumerator<int> customEnumerator = new CustomEnumerator<int>(items);
			var result = customEnumerator.MoveNext();
			Assert.True(result);
		}

		[Theory]
		[InlineData(new[] { 1 })]
		public void CanMoveNextIfNextElementDoesntExists(int[] items) {
			ICustomEnumerator<int> customEnumerator = new CustomEnumerator<int>(items);
			var result = customEnumerator.MoveNext();
			Assert.False(result);
		}

		[Theory]
		[InlineData(new[] { 1, 2, 3, 4, 5, 6 }, 4)]
		public void CanGetCurrentElement(int[] items, int indexForChecking) {
			ICustomEnumerator<int> customEnumerator = new CustomEnumerator<int>(items);
			int loopIterationCount = 0;
			while (customEnumerator.MoveNext() && loopIterationCount != indexForChecking - 1) {
				loopIterationCount++;
			}
			Assert.Equal(items[indexForChecking], customEnumerator.Current);
		}

		[Theory]
		[InlineData(new[] { 1, 2, 3, 4, 5, 6 })]
		[InlineData(new[] { 1 })]
		public void CanReset(int[] items) {
			ICustomEnumerator<int> customEnumerator = new CustomEnumerator<int>(items);
			for (int i = 0; i < items.Length / 2; i++) {
				customEnumerator.MoveNext();
			}
			customEnumerator.Reset();
			Assert.Equal(items[0], customEnumerator.Current);
		}
	}
}
